import type { default as HyperdomeSDKType } from 'hyperdome-sdk'
export const HyperdomeSDK = require('hyperdome-sdk').default as HyperdomeSDKType

export {
  AITModule,
  type AppConfigs,
  Extension,
  ExtensionGroup
} from 'hyperdome'

export interface AITExtensionConfig {
  type?: 'module' | 'extension' | 'extension-group'
  modules?: Array<string>
  files?: Array<string>
}
