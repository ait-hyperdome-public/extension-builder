#!/usr/bin/env node

const path = require('path')
const fs = require('fs')
const net = require('net')
const fse = require('fs-extra')
const cac = require('cac')
const proc = require('child_process')
const electronVite = require('electron-vite')

const cli = cac.cac('extension-builder')

function debounce(func, timeout = 300){
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}

cli
  .command('build', 'build for production')
  .alias('build')
  .option('extension', 'Build Hyperdome extension')
  .option('module', 'Build AIT Module', { default: true })
  .action(function(options) {
    console.log(options)
    if (options.extension) {
      buildExtension()
      return
    }
    if (options.module) {
      buildModule()
    }
  })
cli.command('dev', 'Start Hyperdome in development mode')
  .action(async function() {
    const sock = Math.random() * 1e18
    const port = await getPortFree()
    let server
    let client

    async function getPortFree() {
      return new Promise( res => {
        const srv = net.createServer();
        srv.listen(0, () => {
          const port = srv.address().port
          srv.close((err) => res(port))
        });
      })
    }

    function createServer() {
      return new Promise(function(resolve) {
        const pipe = `\\\\.\\pipe\\${sock}`
        server = net.createServer((stream) => {
          client = stream
          stream.on('data', function(data) {
              console.log(data.toString());
          });
          stream.on('end', () => {
            console.log('[extension-builder] Connection close')
          });
          console.log('[extension-builder] New connection')
        });

        server.on('close', function () {
          console.log('[extension-builder] Server stopped!')
          resolve()
        })
        server.listen(pipe, () => {})
      })
    }

    function startHyperdome() {
      return new Promise(function(resolve) {
        const name = require(path.join(process.cwd(), 'package.json')).name
        const hyperdomeProc = proc.exec(
          `npx electron node_modules/hyperdome/release/app.asar --sock=${sock} --port=${port} --name=${name}`,
        )

        hyperdomeProc.stdout.on('data', console.log)
        hyperdomeProc.stdout.on('error', console.log)
        hyperdomeProc.stdout.on('end', function() {
          if (server) {
            client?.destroy()
            server?.close()
          }
          resolve()
          process.exit()
        })
      })
    }

    function startDev() {
      return electronVite.createServer({ server: { port } }, {})
    }

    function startWatch() {
      return new Promise(function(resolve) {
        setTimeout(function() {
          const watcher = fs.watch(path.join(process.cwd(), 'out'), { recursive: true })
          watcher.on('change', debounce(function(_, filename) {
            console.log('[watcher] file change', filename)
            if (/(main|preload)/.test(filename)) {
              client?.write(JSON.stringify({
                action: 'reload-extension',
                payload: { filename }
              }) + '\n'); // request reload extension
            }
          }))
        }, 1000) // waitting for main application startup
      })
    }

    process.on('beforeExit', function() {
      if (server?.listening) {
        client?.destroy()
        server?.close()
      }
    })

    Promise.all([startDev(), createServer(), startHyperdome(), startWatch()])
  })

cli.help();
cli.version(require('./package.json').version);
cli.parse();

function typeCheck() {
  console.log('[*] Run type check')
  proc.execSync('eslint --ext .js,.cjs,.mjs,.ts,.cts,.mts src/**', { stdio: 'inherit' })
}

function buildSourceCode() {
  console.log('[*] Building...')
  proc.execSync('tsup --config node_modules/extension-builder/dist/tsup.config.ts --dts --dts-resolve', { stdio: 'inherit' })
}

function buildExtension() {
  console.log('[*] Building...')
  electronVite.build()
}

function buildDependences() {
  console.log('[*] Rebuild dependences...')
  const pkg = require(path.join(process.cwd()), 'package.json')
  const electronVersion = require('./package.json').dependencies.electron
  const dependences = pkg.dependences || []
  if (dependences.length) {
    proc.execSync(
      `npx electron-rebuild --version ${electronVersion} -f -w ${dependences.join(',')}`,
      { stdio: 'inherit' }
    )
  }
}

function buildModule() {
  const root = process.cwd()

  console.log('Build AIT Module...')

  typeCheck()
  buildSourceCode()
  // buildDependences()
}

// function buildExtension() {
//   console.log('Build Hyperdome extension...')
//   typeCheck()
//   buildSourceCode()
//   // buildDependences()
// }
