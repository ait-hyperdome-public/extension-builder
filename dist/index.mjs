var __require = /* @__PURE__ */ ((x) => typeof require !== "undefined" ? require : typeof Proxy !== "undefined" ? new Proxy(x, {
  get: (a, b) => (typeof require !== "undefined" ? require : a)[b]
}) : x)(function(x) {
  if (typeof require !== "undefined")
    return require.apply(this, arguments);
  throw new Error('Dynamic require of "' + x + '" is not supported');
});

// src/index.ts
import {
  AITModule,
  Extension,
  ExtensionGroup
} from "hyperdome";
var HyperdomeSDK = __require("hyperdome-sdk").default;
export {
  AITModule,
  Extension,
  ExtensionGroup,
  HyperdomeSDK
};
