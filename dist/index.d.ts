import HyperdomeSDKType from 'hyperdome-sdk';
export { AITModule, AppConfigs, Extension, ExtensionGroup } from 'hyperdome';

declare const HyperdomeSDK: HyperdomeSDKType;

interface AITExtensionConfig {
    type?: 'module' | 'extension' | 'extension-group';
    modules?: Array<string>;
    files?: Array<string>;
}

export { AITExtensionConfig, HyperdomeSDK };
