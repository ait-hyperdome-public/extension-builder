import { defineConfig } from 'tsup'
import bytenode from 'bytenode'
import fs from 'fs'
import path from 'path'
import proc from 'child_process'

export default defineConfig({
  entry: ['./src/index.ts'],
  clean: true,
  dts: true,
  format: ['cjs'],
  external: [/^hyperdome-sdk$/],
  noExternal: [/^hyperdome$/, 'extension-builder'],
  onSuccess: async function() {
    const root = process.cwd()
    const isProductionMode = /production/.test((process.env || {})['NODE_ENV'] || '')

    // Inject Hyperdome SDK
    fs.copyFileSync(
      path.join(root, 'node_modules/hyperdome-sdk/release/hyperdome-sdk.jsc'),
      path.join(root, 'dist', 'hyperdome-sdk.jsc')
    )

    fs.copyFileSync(
      path.join(root, 'node_modules/hyperdome-sdk/release/hyperdome-sdk.electron.jsc'),
      path.join(root, 'dist', 'hyperdome-sdk.electron.jsc')
    )

    const code = fs.readFileSync(path.join(root, 'dist', 'index.js')).toString()
    const injected = code.replace(/hyperdome-sdk/g, './hyperdome-sdk.jsc')
    const injectedForElectron = code.replace(/hyperdome-sdk/g, './hyperdome-sdk.electron.jsc')
    fs.writeFileSync(path.join(root, 'dist', 'index.js'), injected)
    fs.writeFileSync(path.join(root, 'dist', 'index.electron.js'), injectedForElectron)

    await bytenode.compileFile({
      filename: path.join(root, 'dist', 'index.js'),
      output: path.join(root, 'dist', 'index.jsc'),
      compileAsModule: true,
      compress: true
    });

    await bytenode.compileFile({
      filename: path.join(root, 'dist', 'index.electron.js'),
      output: path.join(root, 'dist', 'index-electron.jsc'),
      compileAsModule: true,
      electron: true,
      compress: true
    });

    fs.rmSync(path.join(root, 'dist', 'index.js'))
    fs.rmSync(path.join(root, 'dist', 'index.electron.js'))

    fs.writeFileSync(path.join(root, 'dist', 'index.js'), `require('bytenode')\nmodule.exports = require('./index.jsc')`)
    fs.writeFileSync(path.join(root, 'dist', 'index-electron.js'), `require('bytenode')\nmodule.exports = require('./index-electron.jsc')`)

    if (isProductionMode) {
      if (!fs.existsSync(path.join(root, 'package.json'))) {
        throw new Error('Can not find package.json file')
      }

      const pkg = JSON.parse(fs.readFileSync(path.join(root, 'package.json')).toString())
      delete pkg.devDependencies
      delete pkg.scripts
      delete pkg.repository
      delete pkg.bugs
      delete pkg.homepage
      delete pkg.files

      if (!pkg.dependencies) pkg.dependencies = {}
      pkg.dependencies.bytenode = '^1.5.3'

      fs.writeFileSync(path.join(root, 'dist', 'package.json'), JSON.stringify(pkg, null, 2))
      proc.execSync(`npm install --omit=dev`, { cwd: path.join(root, 'dist') })
    }

    console.log('Done!')
  }
})
